**Part 1. Generic class "ShipWithDroids" and "PriorityQueue" class**  
  
DroidB1ID: 6 ...DROID.. Systems.. READY...  
BattleDroid ID: 0 Ready for battle  
DroidB1ID: 7 ...DROID.. Systems.. READY...  
BattleDroid ID: 3 Ready for battle  
DroidB1ID: 8 ...DROID.. Systems.. READY...  
BattleDroid ID: 10 Ready for battle  
BattleDroid ID: 1 Ready for battle  
DroidB1ID: 9 ...DROID.. Systems.. READY...  
DroidB1ID: 5 ...DROID.. Systems.. READY...  
BattleDroid ID: 2 Ready for battle  
BattleDroid ID: 4 Ready for battle  
  
Priority queue usage:  
BattleDroid ID: 0 Ready for battle  
BattleDroid ID: 1 Ready for battle  
BattleDroid ID: 2 Ready for battle  
BattleDroid ID: 3 Ready for battle  
BattleDroid ID: 4 Ready for battle  
DroidB1ID: 5 ...DROID.. Systems.. READY...  
DroidB1ID: 6 ...DROID.. Systems.. READY...  
DroidB1ID: 7 ...DROID.. Systems.. READY...  
DroidB1ID: 8 ...DROID.. Systems.. READY...  
DroidB1ID: 9 ...DROID.. Systems.. READY...  
BattleDroid ID: 10 Ready for battle  
  
**Part 2. Arrays**  
Array 1: [3, 2, 1, 2, 3, 1]  
Array 2: [3, 2, 4]  
Intersection example:  
[2, 3]  
Symmetric difference example:  
[1, 4]  
  
Task 2. Array with elements that are repeated up to a maximum of twice  
Array:[1, 1, 1, 1, 2, 2, 3, 3, 5, 4, 1, 1, 1, 2]  
If numbers duplicates more than two times:  
[3, 3, 5, 4]  
Removed series of numbers:  
[1, 2, 3, 5, 4, 1, 2]  
  
**Part 3. Game**  
Door #1 	|	Artifact	|	10  
Door #2 	|	Artifact	|	10  
Door #3 	|	Monster 	|	82  
Door #4 	|	Monster 	|	52  
Door #5 	|	Artifact	|	10  
Door #6 	|	Monster 	|	78  
Door #7 	|	Artifact	|	10  
Door #8 	|	Monster 	|	47  
Door #9 	|	Artifact	|	10  
Door #10	|	Artifact	|	10  
Stronger monsters behind the 4 doors  
Sequence to survive:  
[1, 2, 5, 7, 8, 9, 10, 3, 4, 6]  
  
**Part 4. Custom String container**  
ArrayList completed work in 312 ms  
Size of ArrayList: 5000  
CustomStringContainer completed work in 12 ms  
Size of CustomStringContainer: 5000  
  
**Part 5. Country-Capital pairs**  
Unsorted:  
Great Britain: London  
Ukraine: Kyiv  
Poland: Warshava  
Madagascar: Antananarivu  
Portugal: Lisabon  
France: Paris  
Bulgary: Sofia  
Canada: Toronto  
South Korea: Seoul  
Germany: Berlin  
  
Sorted by country with Comparable:  
Bulgary: Sofia  
Canada: Toronto  
France: Paris  
Germany: Berlin  
Great Britain: London  
Madagascar: Antananarivu  
Poland: Warshava  
Portugal: Lisabon  
South Korea: Seoul  
Ukraine: Kyiv  
  
Sorted by capital with Comparator:  
Madagascar: Antananarivu  
Germany: Berlin  
Ukraine: Kyiv  
Portugal: Lisabon  
Great Britain: London  
France: Paris  
South Korea: Seoul  
Bulgary: Sofia  
Canada: Toronto  
Poland: Warshava  
  
Binary search example. Expected: Germany: Berlin  
Found: Germany: Berlin  