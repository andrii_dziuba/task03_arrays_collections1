package part1;

public abstract class AbstractDroid implements Comparable {
    private static int _id = 0;
    protected int id;

    protected AbstractDroid() {
        this.id = _id++;
    }

    @Override
    public int compareTo(Object o) {
        AbstractDroid abstractDroid = (AbstractDroid) o;
        return this.id - abstractDroid.getId();
    }

    protected int getId() {
        return this.id;
    }

    public abstract String toString();
}
