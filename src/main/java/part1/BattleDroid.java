package part1;

public class BattleDroid extends AbstractDroid {

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " ID: " + this.id + " Ready for battle  ";
    }
}
