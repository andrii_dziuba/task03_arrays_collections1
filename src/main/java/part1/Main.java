package part1;

import java.util.Collections;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
//        ShipWithDroids<String> shipWithDroids = new ShipWithDroids<>();   // Compiler error

        ShipWithDroids<AbstractDroid> shipWithDroids = new ShipWithDroids<>();  // OK
        Stream.generate(BattleDroid::new).limit(5).forEach(shipWithDroids::add);
        Stream.generate(DroidB1::new).limit(5).forEach(shipWithDroids::add);

//        shipWithDroids.add(new String()); // Compiler error

        shipWithDroids.add(new BattleDroid());
        Collections.shuffle(shipWithDroids);
        shipWithDroids.forEach(System.out::println);

        // PRIORITY QUEUE USAGE

        System.out.println("\nPriority queue usage:");
        PriorityQueue priorityQueue = new PriorityQueue();
        priorityQueue.addAll(shipWithDroids); // Adding to the queue
        while (!priorityQueue.isEmpty()) {
            System.out.println(priorityQueue.remove());
        }


    }
}
