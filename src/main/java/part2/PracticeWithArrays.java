package part2;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public class PracticeWithArrays {

    enum Action {
        INTERSECTION, SYMMETRIC_DIFFERENCE;
    }

    /**
     * Depends on used <code>Action</code> builds a new array.
     *
     * @param first  first array.
     * @param second second array.
     * @param action INTERSECTION - for build an intersection of two arrays,
     *               SYMMETRIC_DIFFERENCE - for calculate symmetric difference of two arrays.
     * @return a new array.
     */
    public static int[] formNewArrayFrom(int[] first, int[] second, Action action) {
        Stream.of(first, second, action).forEach(Objects::requireNonNull);

        int arrayLength = Math.max(first.length, second.length);
        int[] newArray = new int[arrayLength];
        int iterator = 0;

        Arrays.sort(first);
        Arrays.sort(second);

        first = removeDuplicates(first);
        second = removeDuplicates(second);

        if (action == Action.INTERSECTION) {
            for (int i : first) {
                for (int j : second) {
                    if (i == j) {
                        newArray[iterator++] = i;
                    }
                }
            }
        } else if (action == Action.SYMMETRIC_DIFFERENCE) {
            for (int i : first) {
                boolean isPresent = false;
                for (int j : second) {
                    if (i == j) {
                        isPresent = true;
                    }
                }
                if (!isPresent) {
                    newArray[iterator++] = i;
                }
            }
            for (int i : second) {
                boolean isPresent = false;
                for (int j : first) {
                    if (i == j) {
                        isPresent = true;
                    }
                }
                if (!isPresent) {
                    newArray[iterator++] = i;
                }
            }
        }

        int[] finalArray = new int[iterator];
        System.arraycopy(newArray, 0, finalArray, 0, finalArray.length);
        return finalArray;
    }

    /**
     * Removes duplicate integers from sorted by ascending array.
     *
     * @param arr array to use.
     * @return an array with unique numbers.
     */
    private static int[] removeDuplicates(@NotNull int[] arr) {
        int[] _arr = new int[arr.length];
        int iterator = 0;
        for (int i : arr) {
            boolean isPresent = false;
            for (int j : _arr) {
                if (i == j) {
                    isPresent = true;
                }
            }
            if (!isPresent) {
                _arr[iterator++] = i;
            }
        }
        int[] finalArr = new int[iterator];
        System.arraycopy(_arr, 0, finalArr, 0, finalArr.length);
        return finalArr;
    }

    /**
     * Removes a numbers which are duplicates more than two times.
     *
     * @param array array to examine.
     * @return new array which would contain numbers which duplicates maximum two times.
     */
    public static int[] removeIfNumberDuplicatesMoreThanTwice(@NotNull int[] argArray) {
        if (argArray.length <= 1) {
            return argArray;
        }
        int[] array = new int[argArray.length];
        System.arraycopy(argArray, 0, array, 0, array.length);
        // Additional array for storing numbers, duplicated more than twice
        int[] additionalArray = new int[array.length / 3];
        int iterator = 0;

        Arrays.sort(array);

        int repeats = 1;
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                repeats++;
            } else {
                if (repeats > 2) {
                    additionalArray[iterator++] = array[i - 1];
                }
                repeats = 1;
            }
        }

        int[] _array = new int[array.length - 3 * iterator];
        iterator = 0;
        for (int i : argArray) {
            boolean isPresent = false;
            for (int j : additionalArray) {
                if (i == j) {
                    isPresent = true;
                }
            }
            if (!isPresent) {
                _array[iterator++] = i;
            }
        }
        int[] finalArray = new int[iterator];
        System.arraycopy(_array, 0, finalArray, 0, iterator);
        return finalArray;
    }

    /**
     * Finds a series of same numbers and removes them keeping the last one
     *
     * @param argArray array to examine
     * @return new array with no sequent number repeats
     */
    public static int[] getSeries(int[] argArray) {
        if (argArray.length <= 1) {
            return argArray;
        }
        int[] _array = new int[argArray.length];
        int iterator = 0;

        _array[0] = argArray[0];
        for (int i = 1; i < argArray.length; i++) {
            if (argArray[i] != _array[iterator]) {
                iterator++;
                _array[iterator] = argArray[i];
            }
        }
        int[] finalArray = new int[iterator + 1];
        System.arraycopy(_array, 0, finalArray, 0, iterator + 1);
        return finalArray;
    }

    public static void main(String[] args) {
        // Task 1. Intersection and difference
        int[] experimental1 = new int[]{3, 2, 1, 2, 3, 1};
        int[] experimental2 = new int[]{3, 2, 4};

        System.out.println("Array 1: " + Arrays.toString(experimental1));
        System.out.println("Array 2: " + Arrays.toString(experimental2));

        System.out.println("Intersection example:");
        int[] intersection = formNewArrayFrom(experimental1, experimental2, Action.INTERSECTION);
        System.out.println(Arrays.toString(intersection));

        System.out.println("Symmetric difference example:");
        int[] symmDiff = formNewArrayFrom(experimental1, experimental2, Action.SYMMETRIC_DIFFERENCE);
        System.out.println(Arrays.toString(symmDiff));

        // Task 2. Array with elements that are repeated up to a maximum of twice
        int[] experimental3 = {1, 1, 1, 1, 2, 2, 3, 3, 5, 4, 1, 1, 1, 2};
        System.out.println("\nTask 2. Array with elements that are repeated up to a maximum of twice\n" +
                "Array:" + Arrays.toString(experimental3));
        int[] handledArray = removeIfNumberDuplicatesMoreThanTwice(experimental3);
        System.out.println("If numbers duplicates more than two times:");
        System.out.println(Arrays.toString(handledArray));

        // Task 3. Replace series of numbers by one number
        int[] series = getSeries(experimental3);
        System.out.println("Removed series of numbers:");
        System.out.println(Arrays.toString(series));
    }

}
