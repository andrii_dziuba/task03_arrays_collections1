package part3;

public class Artifact implements Entity {

    private int buff;

    public Artifact(int buff) {
        this.buff = buff;
    }

    @Override
    public int getValue() {
        return buff;
    }

    @Override
    public String toString() {
        return "Artifact";
    }
}
