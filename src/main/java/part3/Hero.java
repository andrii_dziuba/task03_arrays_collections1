package part3;

public class Hero implements Entity {

    private int strength;

    public Hero(int strength) {
        this.strength = strength;
    }

    @Override
    public int getValue() {
        return this.getStrength();
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }
}
