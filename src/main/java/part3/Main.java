package part3;

import java.util.Arrays;

import static java.util.concurrent.ThreadLocalRandom.current;

public class Main {

    private Entity[] behindTheDoors = null;
    private int doorCount = 10;

    private Hero hero = null;

    private int[] openedDoors = null;

    public Main() {
        behindTheDoors = new Entity[doorCount];
        hero = new Hero(25);
        openedDoors = new int[doorCount];
        Arrays.fill(openedDoors, -1);
    }

    /**
     * Randomly generates by uniform distribution law <code>Monster</code>'s and <code>Artifact</code>'s.
     */
    public void generateEntities() {
        for (int i = 0; i < behindTheDoors.length; i++) {
            int rnd = current().nextInt();
            if ((rnd & 1) == 0) {
                behindTheDoors[i] = new Monster(current().nextInt(100 - 5) + 5);
            } else {
                behindTheDoors[i] = new Artifact(current().nextInt(11 - 10) + 10);
            }
        }
    }

    /**
     * Pretty prints the data.
     */
    public void print() {
        int i = 1;
        String format = "%-8s\t|\t%-8s\t|\t%d";
        for (Entity door : behindTheDoors) {
            System.out.println(String.format(format,
                    "Door #" + i++,
                    door.toString(),
                    door.getValue()));
        }
    }

    /**
     * Calculates recursively death doors.
     *
     * @param currentDoor which recursive function checks.
     * @param deathDoors  var for storing.
     * @return count of death doors.
     */
    public int deathDoorCount(int currentDoor, Integer deathDoors) {
        if (currentDoor == doorCount) return deathDoors;

        if (behindTheDoors[currentDoor] instanceof Monster &&
                hero.getStrength() < behindTheDoors[currentDoor].getValue()) {
            deathDoors++;
        }
        return deathDoorCount(currentDoor + 1, deathDoors);
    }

    /**
     * Builds a sequence of door numbers.
     */
    public void openAll() {
        int iterator = 0;
        int cycles = 0;
        do {
            cycles++;
            for (int i = 0; i < behindTheDoors.length; i++) {
                boolean isAlreadyOpened = false;
                for (int opened : openedDoors) {
                    if (opened == i) {
                        isAlreadyOpened = true;
                    }
                }
                if (isAlreadyOpened) {
                    continue;
                }
                Entity door = behindTheDoors[i];
                if (door instanceof Artifact) {
                    hero.setStrength(hero.getStrength() + door.getValue());
                    openedDoors[iterator++] = i;
                } else if (door instanceof Monster) {
                    if (hero.getStrength() >= door.getValue()) {
                        openedDoors[iterator++] = i;
                    }
                }
            }
            if (cycles >= doorCount * 2) break;
        } while (iterator != doorCount);
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.generateEntities();
        main.print();
        int deathDoorCount = main.deathDoorCount(0, new Integer(0));
        System.out.println("Stronger monsters behind the " + deathDoorCount + " doors");
        main.openAll();
        System.out.println("Sequence to survive:");
        if (main.openedDoors[main.doorCount - 1] == -1) {
            System.out.println("No sequence...(((");
        } else {
            System.out.println(Arrays.toString(main.openedDoors));
        }
    }
}
