package part5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Country country = new Country("Germany", "Berlin");
        List<Country> list = new ArrayList<Country>() {{
            add(new Country("Great Britain", "London"));
            add(new Country("Ukraine", "Kyiv"));
            add(new Country("Poland", "Warshava"));
            add(new Country("Madagascar", "Antananarivu"));
            add(new Country("Portugal", "Lisabon"));
            add(new Country("France", "Paris"));
            add(new Country("Bulgary", "Sofia"));
            add(new Country("Canada", "Toronto"));
            add(new Country("South Korea", "Seoul")); // one love ♥♥♥
            add(country);
        }};

        System.out.println("Unsorted:");
        list.forEach(System.out::println);

        Collections.sort(list);
        System.out.println("\nSorted by country:");
        list.forEach(System.out::println);

        list.sort(comparator);
        System.out.println("\nSorted by capital:");
        list.forEach(System.out::println);

        int seek = Collections.binarySearch(list, country, comparator);
        System.out.println("\nBinary search example. Expected: " + country);
        System.out.println("Found: " + list.get(seek));
    }

    static Comparator<Country> comparator = (o1, o2) -> o1.getCapital().compareTo(o2.getCapital());
}
